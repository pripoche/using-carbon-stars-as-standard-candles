# <div align='center'>**Using carbon stars as standard candles**</div>

# I. The luminosity function of carbon stars in the Magellanic Clouds
Luminosity functions are derived for both the Magellanic Clouds and the
Milky Way.

## Abstract
Measuring distances to celestial objects has been one of the most crucial
challenges in astronomy for centuries. Our goal is to derive
a carbon star luminosity function that will eventually be used to determine
distances to galaxies at 50-60 Mpc and hence yield a value of the Hubble
constant. Cool N-type carbon stars exhibit redder near-infrared colours than oxygen-rich stars. Using Two Micron All Sky Survey near-infrared photometry and *Gaia* Data Release 2, we identify carbon stars in the
Magellanic Clouds (MC) and the Milky Way (MW). Carbon stars in the MC appear as a distinct horizontal feature in the near-infrared ((J-K)<sub>0</sub>, M<sub>J</sub>) colour-magnitude diagram. We find the median absolute
magnitude and the dispersion, in the J band, for the Large and the Small Magellanic Clouds
(LMC/SMC). The difference between the MC may be
explained by the lower metallicity of the SMC, but in any case it provides limits on the type
of galaxy whose distance can be determined with this technique. To account for metallicity
effects, we developed a composite magnitude, named C, for which the error-weighted mean
C magnitude of the MC are equal. Thanks to the next generation of telescopes
(*JWST*, *ELT*, *TMT*), carbon stars could be detected in MC-type galaxies at
distances out to 50-60 Mpc. The final goal is to eventually try and improve
the measurement of the Hubble constant and explore the current tensions related
to its value.

## Main features

#### *Catalogues*
Text/CSV files that contain the data (photometry, astrometric parameters, spectroscopy properties of stars) used for this project.

The folder is organized by galaxy name: Large Magellanic Cloud (`LMC`), Small Magellanic Cloud (`SMC`), and Milky Way (`MW`). It also has a folder named `LFs` that contains results used to analyze the luminosity functions. The subfolders are identified after the name of the first author of the external catalogue they contain: [`Kontizas`](https://www.aanda.org/articles/aa/abs/2001/15/aah2365/aah2365.html), [`Alksnis`](https://www.degruyter.com/view/journals/astro/10/1-2/article-p1.xml), [`Li`](https://iopscience.iop.org/article/10.3847/1538-4365/aaa415) or [`Raimondo`](https://www.aanda.org/articles/aa/abs/2005/29/aa1904-04/aa1904-04.html).

Tree of the folder:<br/>
&nbsp;.<br/>
├── [`LFs`](catalogues/LFs)<br/>
├── [`LMC`](catalogues/LMC)<br/>
│&nbsp;&nbsp;&nbsp;└── [`Kontizas`](catalogues/LMC/Kontizas)<br/>
├── [`MW`](catalogues/MW)<br/>
│&nbsp;&nbsp;&nbsp;├── [`Abia`](catalogues/MW/Abia)<br/>
│&nbsp;&nbsp;&nbsp;├── [`Alksnis`](catalogues/MW/Alksnis)<br/>
│&nbsp;&nbsp;&nbsp;├── [`Chen`](catalogues/MW/Chen)<br/>
│&nbsp;&nbsp;&nbsp;├── [`Li`](catalogues/MW/Li)<br/>
│&nbsp;&nbsp;&nbsp;├── [`Suh`](catalogues/MW/Suh)<br/>
│&nbsp;&nbsp;&nbsp;└── [`Whitelock`](catalogues/MW/Whitelock)<br/>
└── [`SMC`](catalogues/SMC)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└── [`Raimondo`](catalogues/SMC/Raimondo)<br/>


The names of the files obey the following nomenclature:
- `name-of-catalogue_catalogue`: original external catalogue. It has a modified version (`..._for2MASS`) that enables a catalogue search in the [2MASS](https://irsa.ipac.caltech.edu/cgi-bin/Gator/nph-dd) data,

- `name-of-galaxy_2MASS_mag`: 2MASS photometry from a cross-match with the original catalogue,

- `name-of-galaxy_2MASS_mag_clean`: the catalogue used to analyse the data (final version),

- `name-of-catalogue_2MASS_mag_Gaia-version_dist`: [for Milky-Way catalogues only] [Gaia](https://gea.esac.esa.int/archive/) astrometric parameters after a cross-match with the catalogue containing 2MASS photometry,

- `name-of-catalogue_2MASS_mag_Gaia-version_dist_3D_reddening`: [for Milky-Way catalogues only] Reddening values from the 3D Dust Mapping by [Green et al. (2019)](http://argonaut.skymaps.info/). This is the catalogue used to analyse the data (final version),

where `name-of-catalogue` can be: `Kontizas`, `Abia`, `Alksnis`, `Chen`, `Li`, `Suh`, `Whitelock` or `Raimondo`; where `Gaia-version` can be: `DR2` or `EDR3`; and where `name-of-galaxy` can be: `LMC`, or `SMC`.

#### *Codes*
All the different codes used throughout this project.

The names of the files obey the following nomenclature:
- `ADQL_query_Gaia-version_name-of-galaxy`(`_name-of-catalogue`)`.sql`: [for the MC] ADQL query used in the Gaia archive to reduce galactic foreground and spurious solutions. [for the Milky Way] ADQL query used in the Gaia archive to obtain astrometric parameters,

- `query_PanSTARRS_3D_dust_map`(`_name-of-catalogue`)`.py`: Python query used to obtain reddening values for stars in the Milky Way,

- `carbon_stars_name-of-galaxy`(`_name-of-catalogue`)`.py`: main codes used to analyse the data in each galaxy and to derive the luminosity functions,

- `carbon_stars_LFs.py`: code used to analyze the luminosity functions and to correct for metallicity in the MC,

- `rename_for_2MASS_name-of-galaxy_name-of-catalogue.py`: Python code that rewrites the original catalogues in the IPAC format (for cross-match with 2MASS data),

where `name-of-catalogue` can be: `Kontizas`, `Alksnis`, `Li`, `Suh` or `Raimondo`; where `Gaia-version` can be: `DR2` or `EDR3`; and where `name-of-galaxy` can be: `LMC`, `MW` or `SMC`.

The other files are from the [qn_stat](https://github.com/fplaza/qn_stat) Python package by Florian Plaza.

#### *Figures*
All the figures saved for the paper. The folder is organized per galaxy (LMC, SMC, and Milky Way). It also has a folder named `LFs` that contains luminosity-function comparisons.

Tree of the folder:<br/>
&nbsp;.<br/>
├── [`LFs`](figures/LFs)<br/>
├── [`LMC`](figures/LMC)<br/>
├── [`MW`](figures/MW)<br/>
└── [`SMC`](figures/SMC)<br/>

## Documentation
Refer to [Ripoche et al. (2020)](https://doi.org/10.1093/mnras/staa1346).

## Licensing, Authors, Acknowledgements
This project was developed by Paul Ripoche in collaboration with Jeremy Heyl,
Javiera Parada and Harvey Richer. Non-software material is distributed under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](https://gitlab.com/pripoche/using-carbon-stars-as-standard-candles/-/blob/master/LICENSE). Software material is distributed under the [GNU General Public Licence V3.0](https://gitlab.com/pripoche/using-carbon-stars-as-standard-candles/-/blob/master/SOFTWARE_LICENSE).
If you have used this work, please cite the corresponding paper in research publications and reports, using the following BibTeX entry:

`
@article{10.1093/mnras/staa1346,
    author = {Ripoche, Paul and Heyl, Jeremy and Parada, Javiera and Richer, Harvey},
    title = "{Carbon stars as standard candles: I. The luminosity function of carbon stars in the Magellanic Clouds}",
    journal = {Monthly Notices of the Royal Astronomical Society},
    volume = {495},
    number = {3},
    pages = {2858-2866},
    year = {2020},
    month = {05},
    issn = {0035-8711},
    doi = {10.1093/mnras/staa1346},
    url = {https://doi.org/10.1093/mnras/staa1346},
    eprint = {https://academic.oup.com/mnras/article-pdf/495/3/2858/33336338/staa1346.pdf},
}
`
