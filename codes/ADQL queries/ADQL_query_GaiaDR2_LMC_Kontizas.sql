SELECT DISTINCT
	gaia.source_id,
	gaia.ra,
	gaia.dec,
	subquery.j_m,
	subquery.j_msigcom,
	subquery.h_m,
	subquery.h_msigcom,
	subquery.k_m,
	subquery.k_msigcom

FROM gaiadr2.gaia_source AS gaia


JOIN (

	SELECT DISTINCT
		tm.source_id,
		kontizas.j_m,
		kontizas.j_msigcom,
		kontizas.h_m,
		kontizas.h_msigcom,
		kontizas.k_m,
		kontizas.k_msigcom

	FROM user_pripoche.kontizas AS kontizas

	JOIN gaiadr2.tmass_best_neighbour AS tm

	ON kontizas.designation = tm.original_ext_source_id

	) AS subquery


ON subquery.source_id = gaia.source_id


WHERE
-- 2MASS completeness
	subquery.j_m < 15.8
	AND subquery.h_m < 15.1
	AND subquery.k_m < 14.3
-- Five-parameter solution accepted
	AND gaia.phot_g_mean_mag < 21
	AND visibility_periods_used >= 6
	AND astrometric_sigma5d_max < 1.2*GREATEST(1, POWER(10, 0.2*(gaia.phot_g_mean_mag -18)))
-- Remove spurious solutions
	AND SQRT(gaia.astrometric_chi2_al/(gaia.astrometric_n_good_obs_al - 5)) < 1.2*GREATEST(1, EXP(-0.2*(gaia.phot_g_mean_mag-19.5)))
	AND (1 + 0.015*POWER(gaia.phot_bp_mean_mag-gaia.phot_rp_mean_mag,2)) < gaia.phot_bp_rp_excess_factor
	AND (1.3 + 0.06*POWER(gaia.phot_bp_mean_mag-gaia.phot_rp_mean_mag,2)) > gaia.phot_bp_rp_excess_factor
-- Reduce galactic foreground
	AND (POWER((gaia.pmra - 1.850), 2) + POWER((gaia.pmdec - 0.234),2)) < 1
	AND gaia.astrometric_matched_observations >= 8
	AND ABS(gaia.parallax_over_error) < 5
	AND parallax_error < 1
	AND ABS(SIN((-0.867666*COS(RADIANS(gaia.ra))-0.198076*SIN(RADIANS(gaia.ra)))*COS(RADIANS(gaia.dec))+0.455984*SIN(RADIANS(gaia.dec)))) > 0.1
	AND ( POWER(gaia.pmra/gaia.pmra_error, 2) + POWER(gaia.pmdec/gaia.pmdec_error, 2) ) > 25
