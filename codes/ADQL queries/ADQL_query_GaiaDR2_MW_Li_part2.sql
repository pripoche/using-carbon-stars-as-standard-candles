SELECT DISTINCT li.source_id, li.lamost, li.raj2000 AS ra, li.dej2000 AS dec, li.sptype, li.jmag, li.hmag, li.ksmag, dist.r_est, dist.r_lo, dist.r_hi
FROM user_pripoche.li_gaia AS li
JOIN external.gaiadr2_geometric_distance AS dist
ON li.source_id = dist.source_id
