-- Using TOPCAT "Query remote ..."
-- IVO ID: "ivo://esavo/gaia/tap"
-- Service URL: https://gea.esac.esa.int/tap-server/tap

SELECT DISTINCT user_table.*, twomass.original_ext_source_id

FROM TAP_UPLOAD.t9 AS user_table

JOIN gaiaedr3.tmass_psc_xsc_best_neighbour AS twomass

ON user_table.source_id = twomass.source_id
