-- Using TOPCAT "Query remote ..."
-- IVO ID: "ivo://uni-heidelberg.de/gaia/tap"
-- Service URL: https://gaia.ari.uni-heidelberg.de/tap

SELECT DISTINCT *

FROM TAP_UPLOAD.t4 AS user_table

JOIN twomass

ON user_table.original_ext_source_id = twomass.mainid

WHERE
-- 2MASS completeness
	jmag <15.85
	AND hmag < 15.1
	AND kmag < 14.3
