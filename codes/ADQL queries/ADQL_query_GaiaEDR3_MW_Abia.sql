-- Using TOPCAT "Query remote ..."
-- IVO ID: "ivo://esavo/gaia/tap"
-- Service URL: https://gea.esac.esa.int/tap-server/tap

SELECT DISTINCT *
    
FROM gaiaedr3.gaia_source AS gaia

JOIN (
	SELECT DISTINCT user_table.*, dr2.dr3_source_id

    	FROM TAP_UPLOAD.t1 AS user_table

    	JOIN gaiaedr3.dr2_neighbourhood AS dr2

    	ON user_table.GDR2 = dr2.dr2_source_id

    	) AS subquery

ON subquery.dr3_source_id = gaia.source_id

JOIN "external".gaiaedr3_distance AS gaia_dist

ON subquery.dr3_source_id = gaia_dist.source_id
