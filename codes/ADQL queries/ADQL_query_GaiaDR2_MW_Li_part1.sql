SELECT DISTINCT gaiadr2.source_id, li.*
FROM gaiadr2.gaia_source AS gaiadr2, user_pripoche.li_carbon_stars AS li
WHERE 1=CONTAINS(
  		POINT('ICRS', li.raj2000, li.dej2000),
  		CIRCLE('ICRS', gaiadr2.ra, gaiadr2.dec,0.2759/360)
		)
