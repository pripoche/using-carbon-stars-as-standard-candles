-- Using TOPCAT "Query remote ..."
-- IVO ID: "ivo://esavo/gaia/tap"
-- Service URL: https://gea.esac.esa.int/tap-server/tap

SELECT DISTINCT *
    
FROM gaiaedr3.gaia_source AS gaia

JOIN (

    SELECT DISTINCT alksnis.*, tm.source_id

    FROM TAP_UPLOAD.t15 AS alksnis

    JOIN gaiaedr3.tmass_psc_xsc_best_neighbour AS tm

    ON alksnis.designation_2MASS = tm.original_ext_source_id

    ) AS subquery

ON subquery.source_id = gaia.source_id

JOIN "external".gaiaedr3_distance AS gaia_dist

ON subquery.source_id = gaia_dist.source_id
