SELECT DISTINCT
	gaia.source_id,
	gaia.ra,
	gaia.dec,
	gaia.parallax,
	gaia.parallax_error,
	gaia_dist.r_est,
	gaia_dist.r_lo,
	gaia_dist.r_hi,
	gaia.phot_g_mean_mag,
	gaia.phot_bp_mean_mag,
	gaia.phot_rp_mean_mag,
	subquery.j_m,
	subquery.j_msigcom,
	subquery.h_m,
	subquery.h_msigcom,
	subquery.k_m,
	subquery.k_msigcom

FROM gaiadr2.gaia_source AS gaia


JOIN (

	SELECT DISTINCT alksnis.*, tm.source_id

	FROM user_pripoche.alksnis AS alksnis

	JOIN gaiadr2.tmass_best_neighbour AS tm

	ON alksnis.designation = tm.original_ext_source_id

	) AS subquery


ON subquery.source_id = gaia.source_id

JOIN external.gaiadr2_geometric_distance AS gaia_dist

ON subquery.source_id = gaia_dist.source_id

WHERE
-- 2MASS completeness
	subquery.j_m < 15.8
	AND subquery.h_m < 15.1
	AND subquery.k_m < 14.3
-- Five-parameter solution accepted
	AND gaia.phot_g_mean_mag < 21
	AND visibility_periods_used >= 6
	AND astrometric_sigma5d_max < 1.2*GREATEST(1, POWER(10, 0.2*(gaia.phot_g_mean_mag -18)))
-- Remove spurious solutions
	AND SQRT(gaia.astrometric_chi2_al/(gaia.astrometric_n_good_obs_al - 5)) < 1.2*GREATEST(1, EXP(-0.2*(gaia.phot_g_mean_mag-19.5)))
	AND (1 + 0.015*POWER(gaia.phot_bp_mean_mag-gaia.phot_rp_mean_mag,2)) < gaia.phot_bp_rp_excess_factor
	AND (1.3 + 0.06*POWER(gaia.phot_bp_mean_mag-gaia.phot_rp_mean_mag,2)) > gaia.phot_bp_rp_excess_factor
