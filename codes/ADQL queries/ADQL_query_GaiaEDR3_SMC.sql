SELECT *

FROM gaiaedr3.tmass_psc_xsc_best_neighbour AS tm

JOIN(
	SELECT *
	FROM gaiaedr3.gaia_source as gaia
	WHERE
		1 = CONTAINS(POINT('ICRS',gaia.ra,gaia.dec), CIRCLE('ICRS',12.80,-73.15,11))
		AND gaia.phot_g_mean_mag < 20.5
		AND gaia.parallax IS NOT NULL
		-- Remove spurious solutions
		AND SQRT(gaia.astrometric_chi2_al/(gaia.astrometric_n_good_obs_al - 5)) < 1.2*GREATEST(1, EXP(-0.2*(gaia.phot_g_mean_mag-19.5)))
		AND (1 + 0.015*POWER(gaia.phot_bp_mean_mag-gaia.phot_rp_mean_mag,2)) < gaia.phot_bp_rp_excess_factor
		AND (1.3 + 0.06*POWER(gaia.phot_bp_mean_mag-gaia.phot_rp_mean_mag,2)) > gaia.phot_bp_rp_excess_factor
		-- Reduce galactic foreground	
		AND (POWER((gaia.pmra - 0.7321), 2) + POWER((gaia.pmdec + 1.2256),2)) < 1
		AND ABS(gaia.parallax_over_error) < 5
		AND gaia.parallax_error < 1
		AND ABS(SIN(RADIANS(gaia.b))) > 0.1
		AND ( POWER(gaia.pmra/gaia.pmra_error, 2) + POWER(gaia.pmdec/gaia.pmdec_error, 2) ) > 25
		) AS gaia_selection

ON gaia_selection.source_id = tm.source_id
