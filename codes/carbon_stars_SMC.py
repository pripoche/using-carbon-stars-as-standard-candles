# %% codecell
import matplotlib.pyplot as plt
import numpy as np
import scipy.optimize as opt
from matplotlib import rc
from qn_calc import qn_calc
from astropy.io import fits

# %% codecell
# Set figures style.
plt.rcParams['figure.dpi'] = 170
plt.rc('font', family='serif')
rc('text', usetex=True)  # latex text rendering
fontsize_label = 14
fontsize_tick = 10
tick_width = 1.2

# %% codecell
# Open the catalogue from the 2MASS query: raw photometry.


def data_raw_generator():
    data_raw = np.genfromtxt(
        '../catalogues/SMC/SMC_2MASS_mag.csv',
        dtype='float',
        delimiter=',',
        names=True,
        invalid_raise=False
        )
    yield data_raw


# %% codecell
# Plot the color-magnitude diagram (CMD) in apparent magnitude.
fig, ax = plt.subplots()
for raw in data_raw_generator():
    plt.plot(
        raw['j_m'] - raw['k_m'], raw['j_m'], linestyle='None',
        markersize=0.1, marker='.', color='black', rasterized=True
        )
    print(len(raw['j_m']))
plt.xlabel(r'$J - K_s$', fontsize=14)
plt.ylabel(r'$J$', fontsize=14)
ax.tick_params(width=1)
plt.setp(ax.get_xticklabels(), fontsize=10)
plt.setp(ax.get_yticklabels(), fontsize=10)
plt.xlim(-1, 3.5)
plt.ylim(17.5, 6)
plt.tight_layout()
# plt.savefig("../figures/SMC/CMD_SMC_2MASS_raw.png", dpi=300)
# plt.savefig("../figures/SMC/CMD_SMC_2MASS_raw.pdf")

# %% codecell
# Open the catalogue from the Gaia query.
# data = np.genfromtxt(
#     '../catalogues/SMC/SMC_2MASS_mag_clean.csv',
#     dtype='float',
#     delimiter=',',
#     names=True,
#     invalid_raise=False
#     )
#
# data.dtype.names

# %% codecell
# Open the catalogue that contains distances and reddening values.
with fits.open(
    '../catalogues/SMC/'
    'SMC_Gaia_EDR3_2MASS_mag_reddening'
    '.fits',
    memmap=True
) as hdul:
    hdul.info()
    hdr = hdul[0].header
    hdu = hdul[1]
    cols = hdu.columns
    data = hdu.data

# %% codecell
# Plot the CMD in apparent magnitude.
fig, ax = plt.subplots()
plt.plot(
    data['jmag'] - data['kmag'], data['jmag'], linestyle='None',
    markersize=0.1, marker='.', color='black',
    rasterized=True
)
plt.xlabel(r'$(J - K_s)_0$', fontsize=14)
plt.ylabel(r'$M_J$', fontsize=14)
ax.tick_params(width=1)
plt.setp(ax.get_xticklabels(), fontsize=10)
plt.setp(ax.get_yticklabels(), fontsize=10)
plt.xlim(-0.5, 3)
plt.ylim(16, 9)
plt.tight_layout()

# %% codecell
# Open the Raimondo catalogue from the Gaia query
data_raimondo = np.genfromtxt(
    '../catalogues/SMC/Raimondo/Raimondo_2MASS_mag_clean.csv',
    dtype='float',
    delimiter=',',
    names=True,
    invalid_raise=False
    )

# %% codecell
# Define absolute magnitudes.

# # Extinction coefficients from [Gordon et al., 2003].
# # mu(SMC) = 18.96 from [Scowcroft et al., 2016].
# M_J = data['j_m'] - 18.96 - 0.131/(1.374-1)*0.084
# M_H = data['h_m'] - 18.96 - 0.169/(1.374-1)*0.084
# M_K = data['k_m'] - 18.96 - 0.016/(1.374-1)*0.084
# J_mag_error = data['j_msigcom']
# M_J_raimondo = data_raimondo['j_m'] - 18.96 - 0.131/(1.374-1)*0.084
# M_H_raimondo = data_raimondo['h_m'] - 18.96 - 0.169/(1.374-1)*0.084
# M_K_raimondo = data_raimondo['k_m'] - 18.96 - 0.016/(1.374-1)*0.084

# Extinction coefficients from [Gordon et al., 2003].
# mu(SMC) = 18.977 from [Graczyk et al., 2020].
# M_J = data['jmag'] - 18.977 - 0.131/(1.374-1)*0.084
# M_H = data['hmag'] - 18.977 - 0.169/(1.374-1)*0.084
# M_K = data['kmag'] - 18.977 - 0.016/(1.374-1)*0.084
M_J_raimondo = data_raimondo['j_m'] - 18.977 - 0.131/(1.374-1)*0.084
M_H_raimondo = data_raimondo['h_m'] - 18.977 - 0.169/(1.374-1)*0.084
M_K_raimondo = data_raimondo['k_m'] - 18.977 - 0.016/(1.374-1)*0.084

# Reddening values from [Skowron et al. 2021].
# http://ogle.astrouw.edu.pl/cgi-ogle/get_ms_ext.py
# A(I) ~= 1.5 E(V-I).
# From [Schlafly and Finkbeiner, 2011], assuming
# R_v = 3.1: A(I) = 1.505 E(B-V)_SFD.
# From [Green et al., 2019](http://argonaut.skymaps.info/usage#units), assuming
# E(B-V)_SFD = 0.884*Bayestar19 or E(B-V)_SFD = 0.996*Bayestar19; we use
# extinction coefficients from Table 1.
# mu(SMC) = 18.977 from [Graczyk et al., 2020].
R_I_SKOWRON = 1.5
SFD_B19 = 0.884
M_J = data['jmag'] - 18.977 - R_I_SKOWRON/(1.505*SFD_B19)*0.7927*data['E(V-I)']
M_H = data['hmag'] - 18.977 - R_I_SKOWRON/(1.505*SFD_B19)*0.4690*data['E(V-I)']
M_K = data['kmag'] - 18.977 - R_I_SKOWRON/(1.505*SFD_B19)*0.3026*data['E(V-I)']

J_mag_error = np.nan_to_num(data['e_jmag'], copy=True, nan=0)

# Define colours.
JK = M_J - M_K
JH = M_J - M_H
HK = M_H - M_K

JK_raimondo = M_J_raimondo - M_K_raimondo
JH_raimondo = M_J_raimondo - M_H_raimondo
HK_raimondo = M_H_raimondo - M_K_raimondo

# Define colour masks.
colour_mask = np.logical_and(1.4 < JK, JK < 2) & (data['jmag'] < 13.3)
colour_mask_raimondo = np.logical_and(1.4 < JK_raimondo, JK_raimondo < 2)

# %% codecell
print(np.median(R_I_SKOWROW/(1.505*SFD_B19)*0.7927*data['E(V-I)'][colour_mask]))
print(np.median(R_I_SKOWROW/(1.505*SFD_B19)*0.4690*data['E(V-I)'][colour_mask]))
print(np.median(R_I_SKOWROW/(1.505*SFD_B19)*0.3026*data['E(V-I)'][colour_mask]))
# %% codecell

# %% codecell
# Cross-match stars between 2MASS query and Kontizas catalogue, within the
# colour range (for GAIA DR2).
# number_cross_match = len(
#     set(
#         data['source_id'][colour_mask]) & set(
#         data_raimondo['source_id'][colour_mask_raimondo]
#         )
#     )
# number_raimondo = len(data_raimondo['source_id'][colour_mask_raimondo])
# number_2MASS = len(data['source_id'][colour_mask])
#
# print(number_2MASS)
# print(number_raimondo)
# print(number_cross_match)

# %% codecell
# Plot the CMD in absolute magnitude.
fig, ax = plt.subplots()
plt.plot(
    JK[colour_mask], M_J[colour_mask], linestyle='None', markersize=0.1, marker='.', color='black',
    rasterized=True
)
plt.xlabel(r'$(J - K_s)_0$', fontsize=14)
plt.ylabel(r'$M_J$', fontsize=14)
ax.tick_params(width=1)
plt.setp(ax.get_xticklabels(), fontsize=10)
plt.setp(ax.get_yticklabels(), fontsize=10)
plt.xlim(-0.5, 3)
plt.ylim(-2.5, -10.5)
plt.tight_layout()
# plt.savefig("../figures/SMC/CMD_SMC_2MASS.png", dpi=300)
# plt.savefig("../figures/SMC/POSTER_CMD_SMC_2MASS.png", dpi=600)
# plt.savefig("../figures/SMC/CMD_SMC_2MASS.pdf")

# %% codecell
# Find the parameters of the underlying normal distribution (of stars)
# by maximizing the likelihood function.


def abs_mag_error(
    mag_err, stat_dist_err, syst_dist_err, A_Av, A_Av_err, Ab_Av, Ab_Av_err,
    colour_excess, colour_excess_err
):
    """Return the error (sigma) in absolute magnitude for a given band and for
    a given galaxy"""
    return np.sqrt(
        stat_dist_err**2  # statistical-distance error
        + syst_dist_err**2  # systematic-distance error
        + (colour_excess/(Ab_Av-1) * A_Av_err)**2  # error from A_K/A_V
        + (A_Av/(Ab_Av-1) * colour_excess_err)**2  # error from E(B-V)
        + (A_Av*colour_excess/(Ab_Av-1)**2 * Ab_Av_err)**2  # err. from A_B/A_V
        + (mag_err)**2  # error from apparent magnitude
        )


def log_inv_likelihood(x, abs_mag, sigma_abs_mag):
    """Return the natural logarithm of the inverse likelihood function"""
    m, sig = x
    log_inv_likelihood = np.sum(
        ((abs_mag-m)**2
            / (2*(sigma_abs_mag**2+sig**2)))
        + 1/2 * np.log(sigma_abs_mag**2+sig**2)
        )
    return log_inv_likelihood


def log_inv_likelihood_qn(x, abs_mag, sigma_abs_mag):
    """Use robust estimator Q_n and return the natural logarithm of the inverse
    likelihood function"""
    m, sig = x
    qn_argument = ((abs_mag - m)/(2*(
        sigma_abs_mag**2 + sig**2
        ))**0.5)
    log_inv_likelihood = len(abs_mag)*(
        qn_calc(qn_argument)**2+np.median(qn_argument)**2
        ) + np.sum(
            1/2*np.log(sigma_abs_mag**2 + sig**2)
            )
    return log_inv_likelihood


res = opt.minimize(
    log_inv_likelihood, (-6.1, 0.37),
    args=(
        M_J[colour_mask],
        abs_mag_error(
            J_mag_error[colour_mask], 0.01, 0.03, 0.131, 0.013, 1.374,
            0.127, 0.084, 0.013
            )
        ),
    method='Nelder-Mead', options={'disp': True}
    )

res_qn = opt.minimize(
    log_inv_likelihood_qn, (-6.1, 0.37),
    args=(
        M_J[colour_mask],
        abs_mag_error(
            J_mag_error[colour_mask], 0.01, 0.03, 0.131, 0.013, 1.374,
            0.127, 0.084, 0.013
            )
        ),
    method='Nelder-Mead', options={'disp': True}
    )

res_raimondo = opt.minimize(
    log_inv_likelihood, (-6.1, 0.37),
    args=(
        M_J_raimondo[colour_mask_raimondo],
        abs_mag_error(
            data_raimondo['j_msigcom'][colour_mask_raimondo], 0.01, 0.03,
            0.131, 0.013, 1.374, 0.127, 0.084, 0.013
            )
        ),
    method='Nelder-Mead', options={'disp': True}
    )

res_qn_raimondo = opt.minimize(
    log_inv_likelihood_qn, (-6.1, 0.37),
    args=(
        M_J_raimondo[colour_mask_raimondo],
        abs_mag_error(
            data_raimondo['j_msigcom'][colour_mask_raimondo], 0.01, 0.03,
            0.131, 0.013, 1.374, 0.127, 0.084, 0.013
            )
        ),
    method='Nelder-Mead', options={'disp': True}
    )

print(res.x)
print(res_qn.x)
print(res_raimondo.x)
print(res_qn_raimondo.x)

# %% codecell
# Save data in order to plot luminosity functions on one graph.
# np.savez(
#     '../catalogues/LFs/SMC_LF.npz',
#     M_J=M_J,
#     M_J_red=data['j_m']-18.96,
#     J_err=data['j_msigcom'],
#     M_K=M_K,
#     K_err=data['k_msigcom'],
#     J_median=res_qn.x[0],
#     J_sigma=res_qn.x[1],
#     JK=JK,
#     JK_red=data['j_m']-data['k_m']
#     )

# %% codecell
# Plot the CMD in absolute magnitude for the Raimondo catalogue.
fig, ax = plt.subplots()
plt.plot(
    JK_raimondo, M_J_raimondo, linestyle='None',
    markersize=0.1, marker='*', color='mediumvioletred'
    )
plt.xlabel(r'$(J - K_s)_0$', fontsize=14)
plt.ylabel(r'$M_J$', fontsize=14)
ax.tick_params(width=1)
plt.setp(ax.get_xticklabels(), fontsize=10)
plt.setp(ax.get_yticklabels(), fontsize=10)
plt.xlim(-0.5, 3)
plt.ylim(-2.5, -10.5)
plt.tight_layout()
# plt.savefig("../figures/SMC/CMD_SMC_Raimondo.png", dpi=300)
# plt.savefig("../figures/SMC/CMD_SMC_Raimondo.eps")

# %% codecell
# Plot both CMDs.
fig, ax = plt.subplots()
plt.plot(
    JK, M_J, linestyle='None', markersize=0.1, marker='.',
    color='black', label='2MASS', rasterized=True
    )
plt.plot(
    JK_raimondo, M_J_raimondo, linestyle='None', markersize=0.1, marker='*',
    color='mediumvioletred', label=r'Raimondo', rasterized=True
    )
plt.axvline(x=1.4, lw=1, linestyle='--', color=(0.8, 0.4, 0))
plt.axvline(x=2, lw=1, linestyle='--', color=(0.8, 0.4, 0))
plt.xlabel(r'$(J - K_s)_0$', fontsize=14)
plt.ylabel(r'$M_J$', fontsize=14)
ax.tick_params(width=1)
plt.setp(ax.get_xticklabels(), fontsize=10)
plt.setp(ax.get_yticklabels(), fontsize=10)
plt.xlim(-0.5, 3)
plt.ylim(-2.5, -10.5)
legend = plt.legend(loc="best", shadow=None, markerscale=60.)
plt.tight_layout()
# plt.savefig("../figures/SMC/CMD_SMC_2MASS_Raimondo.png", dpi=300)
# plt.savefig("../figures/SMC/CMD_SMC_2MASS_Raimondo.pdf")

# %% codecell
# Plot the color-color diagram.
plt.plot(JK, JH, linestyle='None', markersize=0.1, marker='.', color='black')
plt.xlabel(r'$(J - K_s)_0$')
plt.ylabel('$(J - H)_0$')
plt.xlim(-0.5, 3)
plt.ylim(0, 1.5)
plt.tight_layout()
# plt.savefig("../figures/SMC/CCD_SMC_2MASS.png", dpi=300)
# plt.savefig("../figures/SMC/CCD_SMC_2MASS.eps")


# %% codecell
# Plot the luminosity function of the stars within the colour range, and
# its associated normal distribution.
bins = np.arange(-11, 3, 0.05)


def normal_distribution_pdf(x, mu, sig):
    """Return the probability density function of a normal distribution"""
    gaussian = 1 / (sig*np.sqrt(2*np.pi)) * np.exp(-(x-mu)**2/(2*sig**2))
    return gaussian


ax = plt.gca()
plt.hist(
    M_J[colour_mask], bins, histtype='step', color='black', linewidth=1.2,
    density=True, label='data'
    )
plt.plot(
    bins, normal_distribution_pdf(bins, res_qn.x[0], res_qn.x[1]),
    color='black', linestyle='--', linewidth=1.2,
    label='underlying distribution'
    )
plt.xlabel(r'$M_J$', fontsize=14)
plt.ylabel('probability density function (PDF)', fontsize=14)
ax.tick_params(width=1)
plt.setp(ax.get_xticklabels(), fontsize=10)
plt.setp(ax.get_yticklabels(), fontsize=10)
plt.xticks(np.arange(min(bins), max(bins), 1))
legend = plt.legend(
    loc="best", shadow=None,
    title='$\overline{M}_J$=%.3f, $\sigma$=%.3f' % (res_qn.x[0],  res_qn.x[1])
    )
plt.xlim(-3, -9)
plt.tight_layout()
# plt.savefig("../figures/SMC/luminosity_function_SMC_2MASS.png", dpi=300)
# plt.savefig("../figures/SMC/luminosity_function_SMC_2MASS.eps")

# %% codecell
# Plot the luminosity function of the stars within the colour range, and
# its associated normal distribution, for the Raimondo catalogue.
bins = np.arange(-11, 3, 0.05)

ax = plt.gca()
plt.hist(
    M_J_raimondo[colour_mask_raimondo], bins, histtype='step', linewidth=1.2,
    color='mediumvioletred', density=True, label='data'
    )

plt.xlabel(r'$M_J$', fontsize=14)
plt.ylabel('probability density function (PDF)', fontsize=14)
ax.tick_params(width=1)
plt.setp(ax.get_xticklabels(), fontsize=10)
plt.setp(ax.get_yticklabels(), fontsize=10)
plt.xticks(np.arange(min(bins), max(bins), 1))
plt.xlim(-3, -9)
plt.tight_layout()
# plt.savefig("../figures/SMC/luminosity_function_SMC_Raimondo.png", dpi=300)
# plt.savefig("../figures/SMC/luminosity_function_SMC_Raimondo.eps")

# %% codecell
# Plot both luminosity functions of the stars within the colour range.
bins = np.arange(-11, 3, 0.05)
ax = plt.gca()

plt.hist(
    M_J[colour_mask], bins, histtype='step', linestyle='-',
    color='black', linewidth=1.2, density=True,
    label='2MASS (' + str(len(M_J[colour_mask])) + ' stars)'
    )
plt.hist(
    M_J_raimondo[colour_mask_raimondo], bins, histtype='step', linestyle='--',
    linewidth=1.2, color='mediumvioletred', density=True,
    label=r'Raimondo (' + str(len(M_J_raimondo[colour_mask_raimondo])) +
    ' stars)'
    )
plt.xlabel(r'$M_J$', fontsize=14)
plt.ylabel('probability density function (PDF)', fontsize=14)
ax.tick_params(width=1)
plt.setp(ax.get_xticklabels(), fontsize=10)
plt.setp(ax.get_yticklabels(), fontsize=10)
plt.xticks(np.arange(min(bins), max(bins), 1))
legend = plt.legend(loc="best", shadow=None, title='SMC')
plt.xlim(-3, -9)
plt.tight_layout()
# plt.savefig(
#     "../figures/SMC/luminosity_function_SMC_2MASS_Raimondo.png", dpi=300
#     )
# plt.savefig("../figures/SMC/luminosity_function_SMC_2MASS_Raimondo.eps")

# %% codecell
# Get uncertainties on the parameters of the intrinsic distributions using the
# bootstrap method.


def bootstrap_intrinsic_distribution(
    distribution_func, params, abs_mag, J_err
):
    """Yield the result of intrinsic_distribution(), using a bootstrap sample of
    stars"""
    numbers = np.random.randint(
        0, len(abs_mag), len(abs_mag)
        )
    abs_mag = np.asanyarray([abs_mag[i] for i in numbers])
    J_err = np.asanyarray([J_err[i] for i in numbers])
    result = opt.minimize(
        distribution_func, params,
        args=(
            abs_mag,
            abs_mag_error(
                J_err, 0.01, 0.03, 0.131, 0.013, 1.374,
                0.127, 0.084, 0.013
                )
            ),
        method='Nelder-Mead', options={'disp': True}
        )
    yield result


def bootstrap_values(
    distribution_func, params, abs_mag, J_err
):
    """Yield the values of the median and the sigma of the intrisic
    distribution, for a given number of iterations"""
    values = np.asanyarray([[[
            result.x[0], result.x[1]
            ] for result in bootstrap_intrinsic_distribution(
                distribution_func, params, abs_mag, J_err
                )
            ] for i in range(1000)])  # number of iterations
    yield values


def bootstrap_errors(
    distribution_func, params, abs_mag, J_err
):
    """ Return the 68% confidence interval for the median and the sigma of the
    intrinsic distribution"""
    for values in bootstrap_values(
        distribution_func, params, abs_mag, J_err
    ):
        sigma_mean = (np.quantile(values[:, 0, 0], 0.84) - np.quantile(
            values[:, 0, 0], 0.16
            ))/2
        sigma_sigma = (np.quantile(values[:, 0, 1], 0.84) - np.quantile(
            values[:, 0, 1], 0.16
            ))/2
    return [sigma_mean, sigma_sigma]


errors_SMC = bootstrap_errors(
    log_inv_likelihood_qn, (-6.1, 0.37), M_J[colour_mask],
    J_mag_error[colour_mask]
    )

errors_SMC_raimondo = bootstrap_errors(
    log_inv_likelihood_qn, (-6.1, 0.37), M_J_raimondo[colour_mask_raimondo],
    data_raimondo['j_msigcom'][colour_mask_raimondo]
    )

# %% codecell
print('SMC', round(errors_SMC[0], 3), round(errors_SMC[1], 3))
print(
    'SMC_raimondo', round(errors_SMC_raimondo[0], 3),
    round(errors_SMC_raimondo[1], 3)
    )
