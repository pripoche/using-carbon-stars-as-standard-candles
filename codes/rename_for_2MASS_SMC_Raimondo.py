# %% codecell
import numpy as np
from astropy.io import ascii

# %% codecell
# Inport data from the Raimondo catalogue
data = np.genfromtxt(
    '../catalogues/SMC/Raimondo/Raimondo_catalogue.txt',
    dtype='float',
    delimiter='',
    skip_header=42,
    usecols=(2, 3),
    invalid_raise=False,
    names=['ra', 'dec']
    )

data.dtype.names

# %% codecell
# Create a txt file in IPAC format: used to cross-match in 2MASS.
ascii.write(
    [data['ra'], data['dec']],
    '../catalogues/SMC/Raimondo/Raimondo_catalogue_for2MASS.txt',
    names=['ra', 'dec'],
    format='ipac',
    overwrite=True
    )
