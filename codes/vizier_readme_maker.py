import cdspyreadme
from astropy.table import Table
from astropy.io import fits

# %% codecell
# Create tablemaker.
tablemaker = cdspyreadme.CDSTablesMaker()

# %% codecell
object = ['LMC', 'SMC']
description = [
    'catalogue of stars in the Large Magellanic Cloud',
    'catalogue of stars in the Small Magellanic Cloud'
]

EXTENSION = '.fit'

for j in range(len(object)):
    # Create path to access FITS file.
    PATH = '../catalogues/' + object[j] + '/' \
        + object[j] + '_2MASS_mag_clean' + EXTENSION
    # Read FITS file.
    tab = Table.read(PATH)
    # Open FITS file.
    with fits.open(
        PATH,
        memmap=True
    ) as hdul:
        hdul.info()
        hdr = hdul[0].header
        hdu = hdul[1]
        cols = hdu.columns
        data = hdu.data
    # Update description of table columns from FITS header.
    for i in range(len(tab.columns)):
        tab.columns[i].description = hdu.header["TCOMM"+str(i+1)]
    # Add table to list of tables described in README file.
    table = tablemaker.addTable(tab, name=object[j], description=description[j])

# %% codecell
# Write table in CDS-ASCII aligned format (required).
tablemaker.writeCDSTables()

# %% codecell
# Customize ReadMe output
tablemaker.catalogue = ''
tablemaker.title = 'Carbon stars as standard candles: ' \
    'I. The luminosity function of carbon stars in the Magellanic Clouds'
tablemaker.author = 'Ripoche P.'
tablemaker.authors = 'Heyl J., Parada J., Richer H.'
tablemaker.date = 2020
tablemaker.bibcode = '2020MNRAS.495.2858R'
tablemaker.abstract = ''
tablemaker.keywords = 'catalogues – stars: carbon – Hertzsprung–Russell '\
    'and colour–magnitude diagrams – stars: luminosity function, '\
    'mass function – Magellanic Clouds'
tablemaker.putRef(
    'II/246', '2MASS All-Sky Catalog of Point Sources (Cutri+ 2003)'
    )
tablemaker.putRef('I/345', 'Gaia DR2 (Gaia Collaboration, 2018)')

# Print ReadMe
tablemaker.makeReadMe()

# Save ReadMe into a file
with open("../catalogues/LMC/ReadMe", "w") as fd:
    tablemaker.makeReadMe(out=fd)
