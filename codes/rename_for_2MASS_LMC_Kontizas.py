# %% codecell
import numpy as np
from astropy.io import ascii

# %% codecell
# Inport data from the Kontizas catalogue
data = np.genfromtxt(
    '../catalogues/LMC/Kontizas/Kontizas_catalogue.txt',
    dtype='str',
    delimiter=',',
    skip_header=37,
    invalid_raise=False,
    )

# %% codecell
# Extract R.A. and Dec.
ra = [" ".join(data[i].split(" ", 3)[:3]) for i in range(len(data))]

dec = [" ".join(data[i].split(" ", 3)[3:6]) for i in range(len(data))]

# %% codecell
# Create a txt file in IPAC format: used to cross-match in 2MASS.
ascii.write(
    [ra, dec],
    '../catalogues/LMC/Kontizas/Kontizas_catalogue_for2MASS.txt',
    names=['ra', 'dec'],
    format='ipac',
    overwrite=True
    )
