# %% codecell
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rc
from lmfit import Model
import lmfit
from qn_calc import qn_calc

# Figures style.
plt.rcParams['figure.dpi'] = 150
plt.rc('font', family='serif')
rc('text', usetex=False)  # activate latex text rendering

# %% codecell
SMC = np.load('../catalogues/LFs/SMC_LF.npz')
LMC = np.load('../catalogues/LFs/LMC_LF.npz')
MW = np.load('../catalogues/LFs/MW_LF.npz')

colour_mask_LMC = np.logical_and(1.4 < LMC['JK'], LMC['JK'] < 2)
colour_mask_SMC = np.logical_and(1.4 < SMC['JK'], SMC['JK'] < 2)
colour_mask_MW = np.logical_and(1.4 < MW['JK'], MW['JK'] < 2)

# %% codecell
# Define the probability density function of a normal distribution.


def normal_distribution_pdf(x, mu, sig):
    """Return the probability density function of a normal distribution"""
    gaussian = 1 / (sig*np.sqrt(2*np.pi)) * np.exp(-(x-mu)**2/(2*sig**2))
    return gaussian


# %% codecell
# Compare the luminosity functions of the LMC and the SMC.
bins = np.arange(-11, 3, 0.05)
ax = plt.gca()

plt.hist(
    LMC['M_J'][colour_mask_LMC], bins, histtype='step', linestyle='-',
    color='darkmagenta', linewidth=1.2, density=True
    )
plt.plot(
    bins, normal_distribution_pdf(bins, LMC['J_median'], LMC['J_sigma']),
    color='darkmagenta', linestyle='-', linewidth=1,
    label=r'LMC [$\bar{M}_J$=%.3f, $\sigma$=%.3f]'
    % (np.round(LMC['J_median'], 3), np.round(LMC['J_sigma'], 3))
    )
plt.hist(
    SMC['M_J'][colour_mask_SMC], bins, histtype='step', linestyle='--',
    color='darkcyan', linewidth=1.2, density=True
    )
plt.plot(
    bins, normal_distribution_pdf(bins, SMC['J_median'], SMC['J_sigma']),
    color='darkcyan', linestyle='--', linewidth=1,
    label=r'SMC [$\bar{M}_J$=%.3f, $\sigma$=%.3f]'
    % (np.round(SMC['J_median'], 3), np.round(SMC['J_sigma'], 3))
    )
plt.xlabel(r'$M_J$', fontsize=14)
plt.ylabel(r'probability density function (PDF)', fontsize=14)
ax.tick_params(width=1)
plt.setp(ax.get_xticklabels(), fontsize=10)
plt.setp(ax.get_yticklabels(), fontsize=10)
plt.xticks(np.arange(min(bins), max(bins), 1))
legend = plt.legend(loc="upper left", shadow=None)
plt.xlim(-3, -9)
plt.tight_layout()
# plt.savefig("../figures/LFs/POSTER_luminosity_function_SMC_LMC.png", dpi=600)
# plt.savefig("../figures/LFs/luminosity_function_SMC_LMC.eps")

# %% codecell
# Compare the luminosity functions of the LMC, SMC and the Milky Way.
bins = np.arange(-11, 3, 0.05)
ax = plt.gca()

plt.hist(
    LMC['M_J'][colour_mask_LMC], bins, histtype='step', linestyle='-',
    color='darkmagenta', linewidth=1.2, density=True
    )
plt.plot(
    bins, normal_distribution_pdf(bins, LMC['J_median'], LMC['J_sigma']),
    color='darkmagenta', linestyle='-', linewidth=1,
    label=r'LMC [$\bar{M}_J$=%.3f, $\sigma$=%.3f]'
    % (np.round(LMC['J_median'], 3), np.round(LMC['J_sigma'], 3))
    )
plt.hist(
    SMC['M_J'][colour_mask_SMC], bins, histtype='step', linestyle='--',
    color='darkcyan', linewidth=1.2, density=True
    )
plt.plot(
    bins, normal_distribution_pdf(bins, SMC['J_median'], SMC['J_sigma']),
    color='darkcyan', linestyle='--', linewidth=1,
    label=r'SMC [$\bar{M}_J$=%.3f, $\sigma$=%.3f]'
    % (np.round(SMC['J_median'], 3), np.round(SMC['J_sigma'], 3))
    )
plt.hist(
    MW['M_J'][colour_mask_MW], bins, histtype='step', linestyle='-.',
    color='darkorange', linewidth=1.2, density=True
    )
plt.plot(
    bins, normal_distribution_pdf(bins, MW['J_median'], MW['J_sigma']),
    color='darkorange', linestyle='-.', linewidth=1,
    label=r'MW [$\bar{M}_J$=%.3f, $\sigma$=%.3f]'
    % (np.round(MW['J_median'], 3), np.round(MW['J_sigma'], 3))
    )
plt.xlabel(r'$M_J$', fontsize=14)
plt.ylabel(r'probability density function (PDF)', fontsize=14)
ax.tick_params(width=1)
plt.setp(ax.get_xticklabels(), fontsize=10)
plt.setp(ax.get_yticklabels(), fontsize=10)
plt.xticks(np.arange(min(bins), max(bins), 1))
legend = plt.legend(loc="upper left", shadow=None)
plt.xlim(-3, -9)
plt.tight_layout()

plt.savefig("../figures/LFs/luminosity_function_SMC_LMC_MW.png", dpi=300)
plt.savefig("../figures/LFs/luminosity_function_SMC_LMC_MW.eps")

# %% codecell
# Define a linear model in order to do a curve fitting of the carbon-star
# feature in the LMC.


def linear(colour, slope, intercept):
    """Return a linear model"""
    return slope*colour + intercept


# Define errors in absolute magnitude for a given band and a given galaxy.
def abs_mag_error(
    mag_err, stat_dist_err, syst_dist_err, A_Av, A_Av_err, Ab_Av, Ab_Av_err,
    colour_excess, colour_excess_err
):
    """Return the error (sigma) in absolute magnitude for a given band and for
    a given galaxy"""
    return np.sqrt(
        stat_dist_err**2  # statistical-distance error
        + syst_dist_err**2  # systematic-distance error
        + (colour_excess/(Ab_Av-1) * A_Av_err)**2  # error from A_K/A_V
        + (A_Av/(Ab_Av-1) * colour_excess_err)**2  # error from E(B-V)
        + (A_Av*colour_excess/(Ab_Av-1)**2 * Ab_Av_err)**2  # err. from A_B/A_V
        + (mag_err)**2  # error from apparent magnitude
        )


# Curve-fitting of the carbon-star feature in the LMC and SMC. Values are
# taken here for the K_s band.
linear_model = Model(linear)
params = linear_model.make_params(slope=0, intercept=-6)

result_LMC = linear_model.fit(
    LMC['M_J'][colour_mask_LMC],
    params,
    method='nelder',
    calc_covar=True,
    weights=1.0/abs_mag_error(
        LMC['J_err'][colour_mask_LMC], 0.004, 0.026, 0.257, 0.013, 1.293,
        0.113, 0.127, 0.013
        ),
    colour=LMC['JK'][colour_mask_LMC]
    )

result_SMC = linear_model.fit(
    SMC['M_J'][colour_mask_SMC],
    params,
    method='nelder',
    calc_covar=True,
    weights=1.0/abs_mag_error(
        SMC['J_err'][colour_mask_SMC], 0.01, 0.03, 0.131, 0.013, 1.374,
        0.127, 0.084, 0.013
        ),
    colour=SMC['JK'][colour_mask_SMC]
    )

# result_MW = linear_model.fit(
#     MW['M_J'][colour_mask_MW],
#     params,
#     method='nelder',
#     calc_covar=True,
#     weights=1.0/abs_mag_error(
#         MW['J_err'][colour_mask_MW], 0, 0, 0, 0, 0,
#         0, 0, 0
#         ),
#     colour=MW['JK'][colour_mask_MW]
#     )

print('LMC', result_LMC.fit_report())
print('SMC', result_SMC.fit_report())
# print('MW', result_MW.fit_report())

# result_LMC.conf_interval()
# print(result_LMC.ci_report())

# %% codecell
# Calculate the fiducial colour of carbon stars using data in the LMC and SMC
# from the K_s band, and its error.
fiducial_colour = (
    result_SMC.best_values['intercept'] - result_LMC.best_values['intercept']
    ) / (
        result_LMC.best_values['slope'] - result_SMC.best_values['slope']
        )

derivative_slope = (
    result_SMC.best_values['intercept'] - result_LMC.best_values['intercept']
    ) / (
        result_LMC.best_values['slope'] - result_SMC.best_values['slope']
        )**2

derivative_intercept = 1 / (
    result_LMC.best_values['slope'] - result_SMC.best_values['slope']
    )

fiducial_colour_error = np.sqrt(
    result_LMC.covar[0, 0]*derivative_slope**2
    + result_LMC.covar[1, 1]*derivative_intercept**2
    + 2*result_LMC.covar[0, 1]*derivative_slope*derivative_intercept
    + result_SMC.covar[0, 0]*derivative_slope**2
    + result_SMC.covar[1, 1]*derivative_intercept**2
    + 2*result_SMC.covar[0, 1]*derivative_slope*derivative_intercept
    )

print('fiducial colour:', fiducial_colour)
print('error:', fiducial_colour_error)

# %% codecell
# Obtain the errors in the C band.


def C_band_error(
    abs_mag_error, colour, J_abs_mag_error, K_abs_mag_error, slope, slope_error
        ):
    """Return the error (sigma) of the C band for a given galaxy"""
    return np.sqrt(
        abs_mag_error**2
        + (slope*J_abs_mag_error)**2  # distance error is null
        + (slope*K_abs_mag_error)**2  # distance error is null
        + ((colour-fiducial_colour)*slope_error)**2
        # + (slope*fiducial_colour_error)**2
        )


# %% codecell
# Obtain the intrinsic distribution of carbon stars in the C band,
# using lmfit (package).

params_LF = lmfit.Parameters()
params_LF.add_many(('m', -6.1, True, -100, 100), ('sig', 0.4, True, -100, 100))


def log_inv_likelihood(params, abs_mag, colour, sigma_abs_mag, slope):
    """Return the natural logarithm of the inverse likelihood function"""
    m = params['m']
    sig = params['sig']
    C = abs_mag - slope*(colour - fiducial_colour)
    log_inv_likelihood = np.sum(
        ((C-m)**2
            / (2*(sigma_abs_mag**2+sig**2)))
        + 1/2 * np.log(sigma_abs_mag**2+sig**2)
        )
    return log_inv_likelihood


def log_inv_likelihood_qn(params, abs_mag, colour, sigma_abs_mag, slope):
    """Return the natural logarithm of the inverse likelihood function"""
    m = params['m']
    sig = params['sig']
    C = abs_mag - slope*(colour - fiducial_colour)
    qn_argument = ((C - m)/(2*(
        sigma_abs_mag**2 + sig**2
        ))**0.5)
    log_inv_likelihood = len(C)*(
        qn_calc(qn_argument)**2+np.median(qn_argument)**2
        ) + np.sum(
            1/2*np.log(sigma_abs_mag**2 + sig**2)
            )
    return log_inv_likelihood


def intrinsic_distribution(
    distribution_func, params, abs_mag, colour, J_err, K_err, stat_dist_err,
    syst_dist_err, Aj_Av, Aj_Av_err, Ak_Av, Ak_Av_err, Ab_Av, Ab_Av_err,
    colour_excess, colour_excess_err, slope, slope_error
):
    """Return the result of the minimization of the natural logarithm of the
    inverse likelihood function. This result includes the mean and the sigma of
    the intrinsic distribution of carbon stars in the C band"""
    mini = lmfit.Minimizer(
        distribution_func,
        params,
        fcn_args=(
            abs_mag,
            colour,
            C_band_error(
                abs_mag_error(
                    J_err, stat_dist_err, syst_dist_err, Aj_Av, Aj_Av_err,
                    Ab_Av, Ab_Av_err, colour_excess, colour_excess_err
                    ),
                colour,
                abs_mag_error(
                    J_err, 0, 0, Aj_Av, Aj_Av_err, Ab_Av, Ab_Av_err,
                    colour_excess, colour_excess_err
                    ),
                abs_mag_error(
                    K_err, 0, 0, Ak_Av, Ak_Av_err, Ab_Av, Ab_Av_err,
                    colour_excess, colour_excess_err
                    ),
                slope,
                slope_error
                ),
            slope
            ),
        calc_covar=True,
        )
    result = mini.minimize(method='nelder')
    return result


intrinsic_distribution_LMC = intrinsic_distribution(
    log_inv_likelihood_qn, params_LF, LMC['M_J'][colour_mask_LMC],
    LMC['JK'][colour_mask_LMC], LMC['J_err'][colour_mask_LMC],
    LMC['K_err'][colour_mask_LMC], 0.004, 0.026, 0.257, 0.013, 0.030, 0.003,
    1.293, 0.113, 0.127, 0.013, result_LMC.best_values['slope'],
    np.sqrt(result_LMC.covar[0, 0])
    )

intrinsic_distribution_SMC = intrinsic_distribution(
    log_inv_likelihood_qn, params_LF, SMC['M_J'][colour_mask_SMC],
    SMC['JK'][colour_mask_SMC], SMC['J_err'][colour_mask_SMC],
    SMC['K_err'][colour_mask_SMC], 0.01, 0.03, 0.131, 0.013, 0.016, 0.003,
    1.374, 0.127, 0.084, 0.013, result_SMC.best_values['slope'],
    np.sqrt(result_SMC.covar[0, 0])
    )

# intrinsic_distribution_MW = intrinsic_distribution(
#     log_inv_likelihood_qn, params_LF, MW['M_J'][colour_mask_MW],
#     MW['JK'][colour_mask_MW], MW['J_err'][colour_mask_MW],
#     MW['K_err'][colour_mask_MW], 0.5, 0, 0, 0, 0, 0,
#     0, 0, 0, 0, result_MW.best_values['slope'],
#     np.sqrt(result_MW.covar[0, 0])
#     )

print('LMC', lmfit.fit_report(intrinsic_distribution_LMC))
print('SMC', lmfit.fit_report(intrinsic_distribution_SMC))
# print('MW', lmfit.fit_report(intrinsic_distribution_MW))

# # %% codecell
# # Get uncertainties on the parameters of the intrinsic distributions using the
# # bootstrap method.
#
#
# def bootstrap_intrinsic_distribution(
#     distribution_func, params, abs_mag, colour, J_err, K_err, stat_dist_err,
#     syst_dist_err, Aj_Av, Aj_Av_err, Ak_Av, Ak_Av_err, Ab_Av, Ab_Av_err,
#     colour_excess, colour_excess_err, slope, slope_error
# ):
#     """Yield the result of intrinsic_distribution(), using a bootstrap sample of
#     stars"""
#     numbers = np.random.randint(
#         0, len(abs_mag), len(abs_mag)
#         )
#     abs_mag = np.asanyarray([abs_mag[i] for i in numbers])
#     colour = np.asanyarray([colour[i] for i in numbers])
#     K_err = np.asanyarray([K_err[i] for i in numbers])
#     J_err = np.asanyarray([J_err[i] for i in numbers])
#     result = intrinsic_distribution(
#         distribution_func, params, abs_mag, colour, J_err, K_err,
#         stat_dist_err, syst_dist_err, Aj_Av, Aj_Av_err, Ak_Av, Ak_Av_err,
#         Ab_Av, Ab_Av_err, colour_excess, colour_excess_err, slope, slope_error
#         )
#     yield result
#
#
# def bootstrap_values(
#     distribution_func, params, abs_mag, colour, J_err, K_err, stat_dist_err,
#     syst_dist_err, Aj_Av, Aj_Av_err, Ak_Av, Ak_Av_err, Ab_Av, Ab_Av_err,
#     colour_excess, colour_excess_err, slope, slope_error
# ):
#     """Yield the values of the median and the sigma of the intrisic
#     distribution, for a given number of iterations"""
#     values = np.asanyarray([[[
#             result.params.valuesdict()['m'], result.params.valuesdict()['sig']
#             ] for result in bootstrap_intrinsic_distribution(
#                 distribution_func, params, abs_mag, colour, J_err, K_err,
#                 stat_dist_err, syst_dist_err, Aj_Av, Aj_Av_err, Ak_Av,
#                 Ak_Av_err, Ab_Av, Ab_Av_err, colour_excess, colour_excess_err,
#                 slope, slope_error
#                 )
#             ] for i in range(1000)])  # number of iterations
#     yield values
#
#
# def bootstrap_errors(
#     distribution_func, params, abs_mag, colour, J_err, K_err, stat_dist_err,
#     syst_dist_err, Aj_Av, Aj_Av_err, Ak_Av, Ak_Av_err, Ab_Av, Ab_Av_err,
#     colour_excess, colour_excess_err, slope, slope_error
# ):
#     """ Return the 68% confidence interval for the median and the sigma of the
#     intrinsic distribution"""
#     for values in bootstrap_values(
#         distribution_func, params, abs_mag, colour, J_err, K_err,
#         stat_dist_err, syst_dist_err, Aj_Av, Aj_Av_err, Ak_Av, Ak_Av_err,
#         Ab_Av, Ab_Av_err, colour_excess, colour_excess_err, slope, slope_error
#     ):
#         sigma_mean = (np.quantile(values[:, 0, 0], 0.84) - np.quantile(
#             values[:, 0, 0], 0.16
#             )) / 2
#         sigma_sigma = (np.quantile(values[:, 0, 1], 0.84) - np.quantile(
#             values[:, 0, 1], 0.16
#             )) / 2
#     return [sigma_mean, sigma_sigma]
#
#
# errors_LMC = bootstrap_errors(
#     log_inv_likelihood_qn, params_LF, LMC['M_J'][colour_mask_LMC],
#     LMC['JK'][colour_mask_LMC], LMC['J_err'][colour_mask_LMC],
#     LMC['K_err'][colour_mask_LMC], 0.004, 0.026, 0.257, 0.013, 0.030, 0.003,
#     1.293, 0.113, 0.127, 0.013, result_LMC.best_values['slope'],
#     np.sqrt(result_LMC.covar[0, 0])
#     )
#
# errors_SMC = bootstrap_errors(
#     log_inv_likelihood_qn, params_LF, SMC['M_J'][colour_mask_SMC],
#     SMC['JK'][colour_mask_SMC], SMC['J_err'][colour_mask_SMC],
#     SMC['K_err'][colour_mask_SMC], 0.01, 0.03, 0.131, 0.013, 0.016, 0.003,
#     1.374, 0.127, 0.084, 0.013, result_SMC.best_values['slope'],
#     np.sqrt(result_SMC.covar[0, 0])
#     )
#
#
# # %% codecell
# print('LMC', round(errors_LMC[0], 3), round(errors_LMC[1], 3))
# print('SMC', round(errors_SMC[0], 3), round(errors_SMC[1], 3))

# %% codecell
# Compare the luminosity functions of the LMC, SMC and the Milky Way.
bins = np.arange(-11, 3, 0.05)
ax = plt.gca()

C_LMC = LMC['M_J'][colour_mask_LMC] - result_LMC.best_values['slope']*(
    LMC['JK'][colour_mask_LMC] - fiducial_colour
    )

C_SMC = SMC['M_J'][colour_mask_SMC] - result_SMC.best_values['slope']*(
    SMC['JK'][colour_mask_SMC] - fiducial_colour
    )

plt.hist(
    C_LMC, bins, histtype='step', linestyle='-',
    color='darkmagenta', linewidth=1.2, density=True
    )
plt.plot(
    bins, normal_distribution_pdf(
        bins,
        intrinsic_distribution_LMC.params.valuesdict()['m'],
        intrinsic_distribution_LMC.params.valuesdict()['sig']
        ),
    color='darkmagenta', linestyle='-', linewidth=1,
    label=r'LMC [$\bar{C}$=%.3f, $\sigma$=%.3f]'
    % (
        round(intrinsic_distribution_LMC.params.valuesdict()['m'], 3),
        round(intrinsic_distribution_LMC.params.valuesdict()['sig'], 3)
        )
    )
plt.hist(
    C_SMC, bins, histtype='step', linestyle='--',
    color='darkcyan', linewidth=1.2, density=True
    )
plt.plot(
    bins, normal_distribution_pdf(
        bins,
        intrinsic_distribution_SMC.params.valuesdict()['m'],
        intrinsic_distribution_SMC.params.valuesdict()['sig']
        ),
    color='darkcyan', linestyle='--', linewidth=1,
    label=r'SMC [$\bar{C}$=%.3f, $\sigma$=%.3f]'
    % (
        round(intrinsic_distribution_SMC.params.valuesdict()['m'], 3),
        round(intrinsic_distribution_SMC.params.valuesdict()['sig'], 3)
        )
    )
plt.xlabel(r'$C$', fontsize=14)
plt.ylabel(r'probability density function (PDF)', fontsize=14)
ax.tick_params(width=1)
plt.setp(ax.get_xticklabels(), fontsize=10)
plt.setp(ax.get_yticklabels(), fontsize=10)
plt.xticks(np.arange(min(bins), max(bins), 1))
legend = plt.legend(loc="upper left", shadow=None)
plt.xlim(-3, -9)
plt.tight_layout()

plt.savefig("../figures/LFs/luminosity_function_SMC_LMC_Cband.png", dpi=300)
plt.savefig("../figures/LFs/luminosity_function_SMC_LMC_Cband.eps")

# %% codecell
# Get uncertainties on the parameters of the intrinsic distributions using the
# bootstrap method.


def bootstrap_intrinsic_distribution(
    distribution_func, params, abs_mag, colour, J_err, K_err, stat_dist_err,
    syst_dist_err, Aj_Av, Aj_Av_err, Ak_Av, Ak_Av_err, Ab_Av, Ab_Av_err,
    colour_excess, colour_excess_err
):
    """Yield the result of intrinsic_distribution(), using a bootstrap sample of
    stars"""
    numbers = np.random.randint(
        0, len(abs_mag), len(abs_mag)
        )
    abs_mag = np.asanyarray([abs_mag[i] for i in numbers])
    colour = np.asanyarray([colour[i] for i in numbers])
    K_err = np.asanyarray([K_err[i] for i in numbers])
    J_err = np.asanyarray([J_err[i] for i in numbers])
    linear_model = Model(linear)
    params_fit = linear_model.make_params(slope=0, intercept=-6)
    result_fit = linear_model.fit(
        abs_mag,
        params_fit,
        method='nelder',
        calc_covar=True,
        weights=1.0/abs_mag_error(
            J_err, stat_dist_err, syst_dist_err, Aj_Av, Aj_Av_err,
            Ab_Av, Ab_Av_err, colour_excess, colour_excess_err
            ),
        colour=colour
        )
    slope = result_fit.best_values['slope']
    slope_error = np.sqrt(result_fit.covar[0, 0])
    result = intrinsic_distribution(
        distribution_func, params, abs_mag, colour, J_err, K_err,
        stat_dist_err, syst_dist_err, Aj_Av, Aj_Av_err, Ak_Av, Ak_Av_err,
        Ab_Av, Ab_Av_err, colour_excess, colour_excess_err, slope, slope_error
        )
    yield result


def bootstrap_values(
    distribution_func, params, abs_mag, colour, J_err, K_err, stat_dist_err,
    syst_dist_err, Aj_Av, Aj_Av_err, Ak_Av, Ak_Av_err, Ab_Av, Ab_Av_err,
    colour_excess, colour_excess_err
):
    """Yield the values of the median and the sigma of the intrisic
    distribution, for a given number of iterations"""
    values = np.asanyarray([[[
            result.params.valuesdict()['m'], result.params.valuesdict()['sig']
            ] for result in bootstrap_intrinsic_distribution(
                distribution_func, params, abs_mag, colour, J_err, K_err,
                stat_dist_err, syst_dist_err, Aj_Av, Aj_Av_err, Ak_Av,
                Ak_Av_err, Ab_Av, Ab_Av_err, colour_excess, colour_excess_err
                )
            ] for i in range(1000)])  # number of iterations
    yield values


def bootstrap_errors(
    distribution_func, params, abs_mag, colour, J_err, K_err, stat_dist_err,
    syst_dist_err, Aj_Av, Aj_Av_err, Ak_Av, Ak_Av_err, Ab_Av, Ab_Av_err,
    colour_excess, colour_excess_err
):
    """ Return the 68% confidence interval for the median and the sigma of the
    intrinsic distribution"""
    for values in bootstrap_values(
        distribution_func, params, abs_mag, colour, J_err, K_err,
        stat_dist_err, syst_dist_err, Aj_Av, Aj_Av_err, Ak_Av, Ak_Av_err,
        Ab_Av, Ab_Av_err, colour_excess, colour_excess_err
    ):
        sigma_mean = (np.quantile(values[:, 0, 0], 0.84) - np.quantile(
            values[:, 0, 0], 0.16
            )) / 2
        sigma_sigma = (np.quantile(values[:, 0, 1], 0.84) - np.quantile(
            values[:, 0, 1], 0.16
            )) / 2
    return [sigma_mean, sigma_sigma]


errors_LMC = bootstrap_errors(
    log_inv_likelihood_qn, params_LF, LMC['M_J'][colour_mask_LMC],
    LMC['JK'][colour_mask_LMC], LMC['J_err'][colour_mask_LMC],
    LMC['K_err'][colour_mask_LMC], 0.004, 0.026, 0.257, 0.013, 0.030, 0.003,
    1.293, 0.113, 0.127, 0.013
    )

errors_SMC = bootstrap_errors(
    log_inv_likelihood_qn, params_LF, SMC['M_J'][colour_mask_SMC],
    SMC['JK'][colour_mask_SMC], SMC['J_err'][colour_mask_SMC],
    SMC['K_err'][colour_mask_SMC], 0.01, 0.03, 0.131, 0.013, 0.016, 0.003,
    1.374, 0.127, 0.084, 0.013
    )


# %% codecell
print('LMC', round(errors_LMC[0], 3), round(errors_LMC[1], 3))
print('SMC', round(errors_SMC[0], 3), round(errors_SMC[1], 3))
