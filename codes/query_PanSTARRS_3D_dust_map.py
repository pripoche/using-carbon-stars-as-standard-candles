# %% codecell
from astropy import units as u
from astropy.coordinates import SkyCoord
from dustmaps.bayestar import BayestarWebQuery
from dustmaps.sfd import SFDWebQuery
# import numpy as np
# from astropy.io import ascii
from astropy.io import fits

# %% codecell
# Import data from Alksnis catalogue (cross-matched with 2MASS and Gaia).

# data = np.genfromtxt(
#     '../catalogues/MW/Alksnis/Alksnis_2MASS_mag_Gaia_dist.csv',
#     dtype='float',
#     delimiter=',',
#     missing_values='',
#     filling_values=99,
#     invalid_raise=False,
#     names=True,
#     unpack=True
#     )
#
# data.dtype.names

# %% codecell
# Import data from FITS files.
# FILE_NAME = 'Alksnis/Alksnis_2MASS_mag_Gaia_DR2_dist'
# FILE_NAME = 'Alksnis/Alksnis_2MASS_mag_Gaia_EDR3_dist'
# FILE_NAME = 'Abia/Abia_2MASS_mag_Gaia_EDR3_dist'
# FILE_NAME = 'Suh/Suh_table12_2MASS_mag_Gaia_EDR3_dist'
# FILE_NAME = 'Suh/Suh_table10_2MASS_mag_Gaia_EDR3_dist'
# FILE_NAME = 'Li/Li_2MASS_mag_Gaia_EDR3_dist'
# FILE_NAME = 'Whitelock/Whitelock_2MASS_mag_Gaia_EDR3_dist'
FILE_NAME = 'Chen/Chen_2MASS_mag_Gaia_EDR3_dist'

with fits.open(
    '../catalogues/MW/' + FILE_NAME + '.fits',
    memmap=True
) as hdul:
    hdul.info()
    hdr = hdul[0].header
    hdu = hdul[1]
    cols = hdu.columns
    data = hdu.data

# %% codecell
# Choose between GAIA DR2 and GAIA EDR3 distances.
# distance_name = 'r_est'  # GAIA DR2
distance_name = 'r_med_geo'  # GAIA EDR#

# %% codecell
# Define reddening names.
bayestar_reddening_names = [
    'bayestar2019_reddening_16th_pct',
    'bayestar2019_reddening_50th_pct',
    'bayestar2019_reddening_84th_pct'
    ]
bayestar_flag_names = [
    'bayestar2019_flag_converged',
    'bayestar2019_flag_reliable_dist'
    ]
sfd_reddening_name = 'sfd_reddening'

# %% codecell
# Define coordinates.
coords = SkyCoord(
    ra=data['ra']*u.degree, dec=data['dec']*u.degree,
    distance=data[distance_name]*u.pc, frame='icrs'
    )

# %% codecell
# Query the PAN-STARRS 3D dust map [Green et al., 2019].
bayestar = BayestarWebQuery(version='bayestar2019')
bayestar_query = bayestar(
    coords, mode='percentile', pct=(0.16, 0.5, 0.84), return_flags=True
    )

# NAN_VALUE = 99
#
# reddening_values = [
#     np.nan_to_num(result_query[:, i], copy=False, nan=NAN_VALUE)
#     for i in range(3)
#     ]

# %% codecell
# Query the SFD 2D dust map [Schlegel, Finkbeiner & Davis, 1998].
sfd = SFDWebQuery()
sfd_query = sfd(coords)

# %% codecell
# Add reddening values to the Alksnis catalogue and create a csv file.
# column_names = [name for name in data.dtype.names]
# column_names.extend(['e_bv_g2019', 'e_bv_g2019_16th', 'e_bv_g2019_84th'])
#
# data_table = [data[name]for name in data.dtype.names]
# data_table.extend([reddening, reddening_16, reddening_84])

# ascii.write(
#     data_table,
#     '../catalogues/MW/Alksnis/Alksnis_2MASS_mag_Gaia_dist_3D_reddening.csv',
#     names=column_names,
#     format='csv',
#     overwrite=True,
#     )

# # %% codecell
# Create reddening columns for new FITS file.
# https://docs.astropy.org/en/stable/io/fits/usage/table.html#column-creation
bayestar_reddening_columns = [
    fits.Column(
        name=bayestar_reddening_names[i],
        array=bayestar_query[0][:, i],
        format='E',
        unit='mag'
        )
    for i in range(len(bayestar_query[0][0]))
    ]

bayestar_flag_columns = [
    fits.Column(
        name=bayestar_flag_names[i],
        array=bayestar_query[1][bayestar_query[1].dtype.names[i]],
        format='L',
        unit=''
        )
    for i in range(len(bayestar_query[1][0]))
    ]

sfd_reddening_columns = fits.Column(
    name=sfd_reddening_name,
    array=sfd_query,
    format='E',
    unit='mag'
    )

# # %% codecel
# Create column definitions for new FITS file.
# https://docs.astropy.org/
# en/stable/io/fits/api/tables.html#astropy.io.fits.ColDefs
coldefs = fits.ColDefs(data)

# Add reddening columns.
for i in range(len(bayestar_reddening_columns)):
    coldefs.add_col(bayestar_reddening_columns[i])

for i in range(len(bayestar_flag_columns)):
    coldefs.add_col(bayestar_flag_columns[i])

coldefs.add_col(sfd_reddening_columns)

# # %% codecel
# Create new BinTableHDU.
new_hdu = fits.BinTableHDU.from_columns(
    coldefs,
    header=hdu.header  # copy header from original FITS file
    )

# Change name of new BinTableHDU.
new_hdu.name = FILE_NAME.split('/')[1] + '_reddening'

# # %% codecell
# Write new FITS file.
new_hdu.writeto(
    '../catalogues/MW/' + FILE_NAME + '_reddening.fits',
    overwrite=True
    )
