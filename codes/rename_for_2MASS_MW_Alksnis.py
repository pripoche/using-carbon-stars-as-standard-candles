# %% codecell
import numpy as np
from astropy.io import ascii

# %% codecell
# Inport data from the Alksnis catalogue.
data = np.genfromtxt(
    '../catalogues/MW/Alksnis/Alksnis_catalogue.txt',
    dtype=None,  # file is a mix of dtype
    delimiter='|',
    skip_header=20,
    invalid_raise=False,
    names=['number', 'tag', 'count', 'id', 'typ', 'coord'],
    encoding='utf8'
    )

data.dtype.names

# %% codecell
# Extract R.A. and Dec.
ra = [" ".join(data['coord'][i].split(" ", 3)[:3]) for i in range(
    len(data['coord'])
    )]

dec = [" ".join(data['coord'][i].split(" ", 3)[3:5]) for i in range(
    len(data['coord'])
    )]

# %% codecell
# Create a txt file in IPAC format: used to cross-match in 2MASS.
ascii.write(
    [ra, dec],
    '../catalogues/MW/Alksnis/Alksnis_catalogue_for2MASS.txt',
    names=['ra', 'dec'],
    format='ipac',
    overwrite=True
    )
