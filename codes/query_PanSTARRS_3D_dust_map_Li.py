# %% codecell
from astropy.coordinates import SkyCoord
import astropy.units as units
from dustmaps.bayestar import BayestarWebQuery
import numpy as np
from astropy.io import ascii

# %% codecell
# Inport data from Alksnis catalogue (cross-matched with 2MASS and Gaia).
data = np.genfromtxt(
    '../catalogues/MW/Li/Li_2MASS_mag_Gaia_dist.csv',
    dtype=None,  # file is a mix of dtype
    delimiter=',',
    missing_values='',
    filling_values=99,
    invalid_raise=False,
    names=True,
    encoding='utf8'
    )

data.dtype.names

# %% codecell
# Query the PAN-STARRS 3D dust map [Green et al., 2019].
bayestar = BayestarWebQuery(version='bayestar2019')
coords = SkyCoord(
    data['ra']*units.deg, data['dec']*units.deg,
    distance=data['r_est']*units.pc, frame='icrs',
    )

result_query = bayestar(coords, mode='percentile', pct=(0.16, 0.5, 0.84))
result_query = np.where(result_query == 0, 99, result_query)  # missing values
reddening = result_query[:, 1]
reddening_16 = result_query[:, 0]  # 16th percentile of reddening value
reddening_84 = result_query[:, 2]  # 84th percentile of reddening value

# np.nan_to_num(reddening_16, copy=False, nan=99)
# np.nan_to_num(reddening_84, copy=False, nan=99)
# np.nan_to_num(reddening, copy=False, nan=99)

# %% codecell
# Add reddening values to the Alksnis catalogue and create a csv file.
column_names = [name for name in data.dtype.names]
column_names.extend(['e_bv_g2019', 'e_bv_g2019_16th', 'e_bv_g2019_84th'])

data_table = [data[name]for name in data.dtype.names]
data_table.extend([reddening, reddening_16, reddening_84])

ascii.write(
    data_table,
    '../catalogues/MW/Li/Li_2MASS_mag_Gaia_dist_3D_reddening.csv',
    names=column_names,
    format='csv',
    overwrite=True,
    )
