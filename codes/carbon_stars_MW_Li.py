# %% codecell
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rc

# Figures style.
plt.rcParams['figure.dpi'] = 150
plt.rc('font', family='serif')
rc('text', usetex=True)  # activate latex text rendering

# %% codecell
# Open the catalogue from the Gaia query.
data = np.genfromtxt(
        '../catalogues/MW/Li/Li_2MASS_mag_Gaia_dist_3D_reddening.csv',
        dtype=None,  # file is a mix of dtype
        delimiter=',',
        missing_values='',
        filling_values=99,
        invalid_raise=False,
        names=True,
        encoding='utf8'
        )

data.dtype.names

# %% codecell
# Define absolute magnitudes.
M_J = data['jmag'] - 5*np.log10(data['r_est']) + 5 - 0.7927*data['e_bv_g2019']
M_H = data['hmag'] - 5*np.log10(data['r_est']) + 5 - 0.4690*data['e_bv_g2019']
M_K = data['ksmag'] - 5*np.log10(data['r_est']) + 5 - 0.3026*data['e_bv_g2019']

# Define colors.
JK = M_J - M_K
JH = M_J - M_H
HK = M_H - M_K

# %% codecell
# Display the different spectral types from the catalogue.
np.unique(data['sptype'])

# %% codecell
# Plot the CMD in absolute magnitude.
ax = plt.gca()
plt.plot(
    [JK[i] for i in range(len(JK)) if 'C-J' in data['sptype'][i]],
    [M_J[i] for i in range(len(M_J)) if 'C-J' in data['sptype'][i]],
    linestyle='None', markersize=2, marker='s', color='black', label='C-J'
    )
plt.plot(
    [JK[i] for i in range(len(JK)) if 'C-N' in data['sptype'][i]],
    [M_J[i] for i in range(len(M_J)) if 'C-N' in data['sptype'][i]],
    linestyle='None', markersize=2, marker='d', color='mediumvioletred',
    label='C-N'
    )
plt.plot(
    [JK[i] for i in range(len(JK)) if 'Ba' in data['sptype'][i]],
    [M_J[i] for i in range(len(M_J)) if 'Ba' in data['sptype'][i]],
    linestyle='None', markersize=2, marker='v', color='darkorange',
    label='Ba'
    )
plt.plot(
    [JK[i] for i in range(len(JK)) if 'C-R' in data['sptype'][i]],
    [M_J[i] for i in range(len(M_J)) if 'C-R' in data['sptype'][i]],
    linestyle='None', markersize=2, marker='o', color='darkcyan', label='C-R'
    )
plt.plot(
    [JK[i] for i in range(len(JK)) if 'C-H' in data['sptype'][i]],
    [M_J[i] for i in range(len(M_J)) if 'C-H' in data['sptype'][i]],
    linestyle='None', markersize=2, marker='>', color='olive', label='C-H'
    )
plt.axvline(x=1.4, lw=1, linestyle='--', color=(0.8, 0.4, 0))
plt.axvline(x=2, lw=1, linestyle='--', color=(0.8, 0.4, 0))
plt.xlabel(r'$(J - K_s)_0$', fontsize=14)
plt.ylabel(r'$M_J$', fontsize=14)
ax.tick_params(width=1)
plt.setp(ax.get_xticklabels(), fontsize=10)
plt.setp(ax.get_yticklabels(), fontsize=10)
plt.xlim(0, 3)
plt.ylim(8, -10.5)
legend = plt.legend(
    loc="best", shadow=None, markerscale=2
    )
plt.tight_layout()
plt.savefig("../figures/MW/CMD_Li_spectral_type.png", dpi=300)
plt.savefig("../figures/MW/CMD_Li_spectral_type.eps")
