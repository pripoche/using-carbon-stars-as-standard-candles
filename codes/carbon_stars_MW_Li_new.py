# %% codecell
import matplotlib.pyplot as plt
import numpy as np
import scipy.optimize as opt
from matplotlib import rc
from qn_calc import qn_calc

from astropy.io import fits

# %% codecell
# Set figures style.
plt.rcParams['figure.dpi'] = 170
plt.rc('font', family='serif')
rc('text', usetex=False)  # latex text rendering
fontsize_label = 14
fontsize_tick = 10
tick_width = 1.2

# %% codecell
# Open the catalogue that contains distances and reddening values.
# data = np.genfromtxt(
#     '../catalogues/MW/Alksnis/Alksnis_2MASS_mag_Gaia_dist_3D_reddening.csv',
#     dtype='float',
#     delimiter=',',
#     names=True,
#     invalid_raise=False
#     )
#
# data.dtype.names

with fits.open(
    '../catalogues/MW/Li/'
    'Li_2MASS_mag_Gaia_EDR3_dist_3D_reddening'
    '.fits',
    memmap=True
) as hdul:
    hdul.info()
    hdr = hdul[0].header
    hdu = hdul[1]
    cols = hdu.columns
    data = hdu.data
# %% codecell
np.unique(data['sptype'])

# %% codecell
# Select spectral type for carbon stars.

# spectral_type_mask = [
#     True if
#     ('C-N' in data['sptype'][i])
#     # or ('C-J(N)' == data['sptype'][i])
#     # or ('C-J(UNKNOWN)' in data['sptype'][i])
#     else False for i in range(len(data))
#     ]
# data = data[spectral_type_mask]
# len(data)

# %% codecell
# GAIA DR2 data.

# Remove missing reddening values.
# reliable_reddening_mask = np.logical_and(
#     data['e_bv_g2019_84th'] != 99,
#     data['e_bv_g2019_16th'] != 99
#     ) & (data['e_bv_g2019'] != 99)

# data = data[reliable_reddening_mask]  # remove missing values
# print(len(data))

# Name useful variables.
# reddening = data['e_bv_g2019']
# reddening_low = data['e_bv_g2019_16th']
# reddening_high = data['e_bv_g2019_84th']
# distance = data['r_est']
# distance_low = data['r_lo']
# distance_high = data['r_hi']

# Define absolute magnitudes.
# M_J = data['j_m'] - 5*np.log10(distance) + 5 - 0.7927*reddening
# M_H = data['h_m'] - 5*np.log10(distance) + 5 - 0.4690*reddening
# M_K = data['k_m'] - 5*np.log10(distance) + 5 - 0.3026*reddening
# J_mag_error = data['j_msigcom']

# %% codecell
# GAIA EDR3 data.

# Remove missing reddening values.
reliable_reddening_mask = np.logical_and(
    data['e_bv_g2019_EDR3'] != 99,
    data['e_bv_g2019_16th_EDR3'] != 99
    ) & (data['e_bv_g2019_84th_EDR3'] != 99)

# Lee et al. (2021) cut.
# reliable_astrometry_mask = np.logical_and(
#     data['ruwe'] < 2,
#     data['parallax_over_error'] > 0.20
#     )

# Five-parameter-solution cut (GAIA EDR3).
reliable_astrometry_mask = (data['astrometric_params_solved'] == 31)

data = data[reliable_reddening_mask & reliable_astrometry_mask]
print(len(data))

# Name useful variables.
reddening = data['e_bv_g2019_EDR3']
reddening_low = data['e_bv_g2019_16th_EDR3']
reddening_high = data['e_bv_g2019_84th_EDR3']
distance = data['r_med_geo']
distance_low = data['r_lo_geo']
distance_high = data['r_hi_geo']

# Define absolute magnitudes.
M_J = data['jmag'] - 5*np.log10(distance) + 5 - 0.7927*reddening
M_H = data['hmag'] - 5*np.log10(distance) + 5 - 0.4690*reddening
M_K = data['kmag'] - 5*np.log10(distance) + 5 - 0.3026*reddening
J_mag_error = data['e_jmag']

# %% codecell
# Define colors.
JK = M_J - M_K
JH = M_J - M_H
HK = M_H - M_K

# %% codecell
# Plot the CMD in absolute magnitude.
fig, ax = plt.subplots()
plt.plot(
    JK, M_J, linestyle='None', markersize=0.5, marker='.', color='black'
    )
plt.axvline(x=1.4, lw=1, linestyle='--', color=(0.8, 0.4, 0))
plt.axvline(x=2, lw=1, linestyle='--', color=(0.8, 0.4, 0))
plt.xlabel(r'$(J - K_s)_0$', fontsize=14)
plt.ylabel(r'$M_J$', fontsize=14)
ax.tick_params(width=1)
plt.setp(ax.get_xticklabels(), fontsize=10)
plt.setp(ax.get_yticklabels(), fontsize=10)
plt.xlim(-0.5, 3)
plt.ylim(-2.5, -10.5)
plt.tight_layout()
# plt.savefig("../figures/MW/CMD_Alksnis.png", dpi=300)
# plt.savefig("../figures/MW/CMD_Alksnis.eps")

# %% codecell
# Find the parameters of the underlying normal distribution (of stars)
# by maximizing the likelihood function.
colour_mask = np.logical_and(1.4 < JK, JK < 2)


def abs_mag_error(
    mag_err, r_hi, r_lo, e_bv_g2019_84th, e_bv_g2019_16th
):
    """Return the error (sigma) in absolute magnitude for a given band and for
    a given galaxy"""
    return np.sqrt(
        ((5*np.log10(r_hi) - 5*np.log10(r_lo)) / 2)**2
        + ((0.7927*e_bv_g2019_84th - 0.7927*e_bv_g2019_16th) / 2)**2
        + mag_err**2
        )


def log_inv_likelihood(x, abs_mag, sigma_abs_mag):
    """Return the natural logarithm of the inverse likelihood function"""
    m, sig = x
    log_inv_likelihood = np.sum(
        ((abs_mag-m)**2
            / (2*(sigma_abs_mag**2+sig**2)))
        + 1/2 * np.log(sigma_abs_mag**2+sig**2)
        )
    return log_inv_likelihood


def log_inv_likelihood_qn(x, abs_mag, sigma_abs_mag):
    """Use robust estimator Q_n and return the natural logarithm of the inverse
    likelihood function"""
    m, sig = x
    qn_argument = ((abs_mag - m)/(2*(
        sigma_abs_mag**2 + sig**2
        ))**0.5)
    log_inv_likelihood = len(abs_mag)*(
        qn_calc(qn_argument)**2+np.median(qn_argument)**2
        ) + np.sum(
            1/2*np.log(sigma_abs_mag**2 + sig**2)
            )
    return log_inv_likelihood


# %% codecell
res = opt.minimize(
    log_inv_likelihood, (-6.1, 0.37),
    args=(
        M_J[colour_mask],
        abs_mag_error(
            J_mag_error[colour_mask], distance_high[colour_mask],
            distance_low[colour_mask], reddening_high[colour_mask],
            reddening_low[colour_mask]
            )
        ),
    method='Nelder-Mead', options={'disp': True}
    )

res_qn = opt.minimize(
    log_inv_likelihood_qn, (-6.1, 0.37),
    args=(
        M_J[colour_mask],
        abs_mag_error(
            J_mag_error[colour_mask], distance_high[colour_mask],
            distance_low[colour_mask], reddening_high[colour_mask],
            reddening_low[colour_mask]
            )
        ),
    method='Nelder-Mead', options={'disp': True}
    )

print(res.x)
print(res_qn.x)
print(len(M_J[colour_mask]))
# %% codecell
# Save data in order to plot luminosity functions on one graph.
# np.savez(
#     '../catalogues/LFs/MW_LF.npz',
#     M_J=M_J,
#     J_err=data['j_msigcom'],
#     M_K=M_K,
#     K_err=data['k_msigcom'],
#     J_median=res_qn.x[0],
#     J_sigma=res_qn.x[1],
#     JK=JK
#     )

# %% codecell
# Plot the luminosity function of the stars within the colour range, and
# its associated normal distribution.
bins = np.arange(-11, 3, 0.1)


def normal_distribution_pdf(x, mu, sig):
    """Return the probability density function of a normal distribution"""
    gaussian = 1 / (sig*np.sqrt(2*np.pi)) * np.exp(-(x-mu)**2/(2*sig**2))
    return gaussian


ax = plt.gca()
plt.hist(
    M_J[colour_mask], bins, histtype='step', color='black', linewidth=1.2,
    density=True, label='data'
    )
plt.plot(
    bins, normal_distribution_pdf(bins, res_qn.x[0], res_qn.x[1]),
    color='black', linestyle='--', linewidth=1.2,
    label='underlying distribution'
    )
plt.xlabel(r'$M_J$', fontsize=14)
plt.ylabel('probability density function (PDF)', fontsize=14)
ax.tick_params(width=1)
plt.setp(ax.get_xticklabels(), fontsize=10)
plt.setp(ax.get_yticklabels(), fontsize=10)
plt.xticks(np.arange(min(bins), max(bins), 1))
legend = plt.legend(
    loc="best", shadow=None,
    title=r'$\overline{M}_J$=%.3f, $\sigma$=%.3f'
    % (res_qn.x[0],  res_qn.x[1]))
plt.xlim(-3, -9)
plt.tight_layout()
# plt.savefig("../figures/MW/luminosity_function_MW_2MASS.png", dpi=300)
# plt.savefig("../figures_new/luminosity_function_MW_2MASS.eps")

# %% codecell
# Get uncertainties on the parameters of the intrinsic distributions using the
# bootstrap method.


def bootstrap_intrinsic_distribution(
    distribution_func, params, abs_mag, J_err, r_hi, r_lo, e_bv_g2019_84th,
    e_bv_g2019_16th
):
    """Yield the result of intrinsic_distribution(), using a bootstrap sample of
    stars"""
    numbers = np.random.randint(
        0, len(abs_mag), len(abs_mag)
        )
    abs_mag = np.asanyarray([abs_mag[i] for i in numbers])
    J_err = np.asanyarray([J_err[i] for i in numbers])
    r_hi = np.asanyarray([r_hi[i] for i in numbers])
    r_lo = np.asanyarray([r_lo[i] for i in numbers])
    e_bv_g2019_84th = np.asanyarray([e_bv_g2019_84th[i] for i in numbers])
    e_bv_g2019_16th = np.asanyarray([e_bv_g2019_16th[i] for i in numbers])
    result = opt.minimize(
        distribution_func, params,
        args=(
            abs_mag,
            abs_mag_error(
                J_err, r_hi, r_lo, e_bv_g2019_84th, e_bv_g2019_16th
                )
            ),
        method='Nelder-Mead', options={'disp': True}
        )
    yield result


def bootstrap_values(
    distribution_func, params, abs_mag, J_err, r_hi, r_lo, e_bv_g2019_84th,
    e_bv_g2019_16th
):
    """Yield the values of the median and the sigma of the intrisic
    distribution, for a given number of iterations"""
    values = np.asanyarray([[[
            result.x[0], result.x[1]
            ] for result in bootstrap_intrinsic_distribution(
                distribution_func, params, abs_mag, J_err, r_hi, r_lo,
                e_bv_g2019_84th, e_bv_g2019_16th
                )
            ] for i in range(1000)])  # number of iterations
    yield values


def bootstrap_errors(
    distribution_func, params, abs_mag, J_err, r_hi, r_lo, e_bv_g2019_84th,
    e_bv_g2019_16th
):
    """ Return the 68% confidence interval for the median and the sigma of the
    intrinsic distribution"""
    for values in bootstrap_values(
        distribution_func, params, abs_mag, J_err, r_hi, r_lo, e_bv_g2019_84th,
        e_bv_g2019_16th
    ):
        sigma_mean = (np.quantile(values[:, 0, 0], 0.84) - np.quantile(
            values[:, 0, 0], 0.16
            ))/2
        sigma_sigma = (np.quantile(values[:, 0, 1], 0.84) - np.quantile(
            values[:, 0, 1], 0.16
            ))/2
    return [sigma_mean, sigma_sigma]


errors_MW = bootstrap_errors(
    log_inv_likelihood_qn, (-6.1, 0.37), M_J[colour_mask],
    J_mag_error[colour_mask], distance_high[colour_mask],
    distance_low[colour_mask], reddening_high[colour_mask],
    reddening_low[colour_mask]
    )

# %% codecell
print('MW', round(errors_MW[0], 3), round(errors_MW[1], 3))
